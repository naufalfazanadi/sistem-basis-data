/* =========== BISMILLAH~ ========== */
/* Muhammad Naufal Fazanadi | 1505439 */

/* ---- INIT ---- */
connect system;

create user praktikum3 identified by praktikum3;

grant connect to praktikum3;

grant all privileges to praktikum3;

connect praktikum3;
/* ---- ----- */

/* OUTPUT */
set serveroutput on;

BEGIN
  DBMS_OUTPUT.PUT_LINE('Hello World!');
END;
/

/* KAYAK PASCAL */
DECLARE
  bilangan number;
  teks varchar2(20);
BEGIN
  bilangan := 10;
  teks := 'Belajar Oracle'; -- gaboleh " " harus ' '

  DBMS_OUTPUT.PUT_LINE(bilangan);
  DBMS_OUTPUT.PUT_LINE(teks);
END;
/
-- ------------------------------------------------
  v_counter number; -- v_ = var
  v_nama varchar2(20);
  v_nim mahasiswa.nim%type; -- mengambil tipe data dari kolom nim tabel mahasiswa
-- ------------------------------------------------

/* -- LATIHAN -- */
create table coba1(
  nilai number(3)
);

create table coba2(
  nilai number(3)
);

DECLARE
  v_counter NUMBER(3);
BEGIN
  v_counter := 0;
  LOOP
    INSERT INTO coba1(nilai) VALUES (v_counter);
    v_counter := v_counter + 1;
    IF (v_counter > 10) THEN
      EXIT;
    END IF;
  END LOOP;
END;
/

/* PROCEDURE */
CREATE OR REPLACE PROCEDURE prc_isi_deret AS
  v_counter NUMBER(3);
BEGIN
  v_counter := 0;

  LOOP
    DBMS_OUTPUT.PUT_LINE('Nilai counter saat ini : ' || v_counter); -- menampilkan var v_counter
    INSERT INTO coba2(nilai) VALUES (v_counter);

    v_counter := v_counter + 1;

    IF (v_counter > 10) THEN
      EXIT;
    END IF;

  END LOOP;
END prc_isi_deret;
/

set serveroutput on;
EXEC prc_isi_deret; -- menjalan kan procedure
DROP PROCEDURE prc_isi_deret;

/* STRUKTUR LOOP */

LOOP
  -- PROGRAM
  EXIT WHEN kondisi;
END LOOP;

/* WHILE */

WHILE <konsisi> LOOP
  -- PROGRAM
  EXIT WHEN kondisi;
END LOOP;

/* FOR */

counter := 0;
FOR counter IN REVERSE 0..100 BY 1 LOOP
  -- program
END LOOP;

/* PENGGUNAAN IN */
CREATE OR REPLACE PROCEDURE prc_contoh_1 (v_batas IN NUMBER) AS
  v_counter NUMBER;
BEGIN
  v_counter := 0;
  LOOP
    INSERT INTO coba1(nilai) VALUES (v_counter);
    v_counter := v_counter + 1;

    EXIT WHEN v_counter > v_batas;
  END LOOP;
END prc_contoh_1;
/

/* IN AND OUT */
CREATE OR REPLACE PROCEDURE prc_contoh_2 (v_batas IN NUMBER, v_keluaran OUT NUMBER) AS
  v_counter NUMBER;
BEGIN
  v_counter := 0;
  LOOP
    INSERT INTO coba1(nilai) VALUES (v_counter);
    v_counter := v_counter + 2;

    EXIT WHEN v_counter > v_batas;
  END LOOP;
  v_keluaran := v_counter;
END prc_contoh_2;
/

DECLARE
  v_keluaran NUMBER;
BEGIN
  prc_contoh_2(10, v_keluaran);
  DBMS_OUTPUT.PUT_LINE('Counter terakhir : ' || v_keluaran);
END;

/* ---------------------------------- BONUS ---------------------------------- */
CREATE OR REPLACE PROCEDURE prc_latihan (n IN NUMBER) AS
  i NUMBER;
  j NUMBER;
BEGIN
  i := 0;
  LOOP
    j := 0;
    LOOP
      DBMS_OUTPUT.PUT(n);
      j := j + 1;
      EXIT WHEN j > i;
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('');

    i := i + 1;
    EXIT WHEN i >= n;
  END LOOP;
END prc_latihan;
/

DECLARE
  n NUMBER;
BEGIN
  prc_latihan(5);
END;
/