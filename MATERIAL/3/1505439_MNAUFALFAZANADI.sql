/* =========== BISMILLAH~ ========== */
/* Muhammad Naufal Fazanadi | 1505439 */

/* ---- INIT ---- */
connect system

create user function identified by function;

grant connect to function;

grant all privileges to function;

connect function;
/* ------------- */

-- WANT TO DROP ? $DROP USER TP1 CASCADE;

/* ---------- FUNCTION --------- */
create table latihan_function (
  nilai number(3) not null
);

insert all
  into latihan_function values (35)
  into latihan_function values (15)
  into latihan_function values (10)
  into latihan_function values (22)
  into latihan_function values (21)
  into latihan_function values (78)
  into latihan_function values (32)
  select * from dual;

CREATE OR REPLACE FUNCTION l_function return number AS
  nilai_max integer;
BEGIN
  select max(nilai) into nilai_max from latihan_function;
  return (nilai_max);
END l_function;
/

/* HAPUS FUNCTION */
drop function l_function;

/* ------------- CURSOR ------------ */
-- cursor
DECLARE
  c_nilai1 latihan_function.nilai%type;
  c_nilai2 latihan_function.nilai%type;
BEGIN
  c_nilai2 := 78;
  select nilai into c_nilai1 from latihan_function where nilai = c_nilai2;
  dbms_output.put_line('Nilai :' || c_nilai2);
END;
/

--explicit cursor
DECLARE
  c_nilai1 latihan_function.nilai%type;
  cursor cursor_2 is select nilai from latihan_function;
BEGIN
  open cursor_2;
  loop
    fetch cursor_2 into c_nilai1;
    exit when cursor_2%notfound;

  dbms_output.put_line('Nilai'|| c_nilai1);

  end loop;
  close cursor_2; -- dont forget to close

END;
/

-- cursor 2
create table laihan_cursor2(
  nilai number(3) primary key not null,
  komentar varchar(15)
);

insert all
  into latihan_cursor2 values ('100','')
  into latihan_cursor2 values ('80','')
  into latihan_cursor2 values ('70','')
  into latihan_cursor2 values ('50','')
  select * from dual;

create or replace procedure prc_latcursor as
  v_nilai latihan_cursor2.nilai%type;
  v_komentar latihan_cursor2.komentar%type;
  cursor c_nilai is select nilai from latihan_cursor2;
BEGIN
  open c_nilai;
  loop
    fetch c_nilai into v_nilai;
  exit when c_nilai%notfound;

    if v_nilai > 85 then
    v_komentar := 'A';
    elsif v_nilai > 75 then
    v_komentar := 'B';
    elsif v_nilai > 65 then
    v_komentar := 'C';
    elsif v_nilai > 55 then
    v_komentar := 'D';
    else
    v_komentar := 'E';

END;
/