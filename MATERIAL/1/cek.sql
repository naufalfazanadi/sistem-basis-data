/* =========== BISMILLAH~ ========== */
/* Muhammad Naufal Fazanadi | 1505439 */

/* ---- INIT ---- */
connect system

create user praktikum2 identified by prak2;

grant connect to praktikum2;

grant all privileges to praktikum2;

connect praktikum2;
/* ---- ----- */

create table admin(
  ID_admin number(11),
  nama_admin varchar2(100),
  username varchar2(20),
  password varchar2(100),
  created_at timestamp
);

select * from tab;
select * from admin;

/* INLINE + CONSTRAINT */
create table admin(
  ID_admin number(11) not null,
  nama_admin varchar2(100) not null,
  username varchar2(20) unique,
  password varchar2(100) not null,
  created_at timestamp
);

/* OUT-LINE + CONSTRAINT */
create table admin(
  ID_admin number(11) not null,
  nama_admin varchar2(100) not null,
  username varchar2(20),
  password varchar2(100) not null,
  created_at timestamp,
  constraint u_pass unique (username)
);

/* PRIMARY KEY */
/* INLINE + CONSTRAINT */
create table admin(
  ID_admin number(11) primary key,
  nama_admin varchar2(100) not null,
  username varchar2(20) unique,
  password varchar2(100) not null,
  created_at timestamp
);

/* OUT-LINE + CONSTRAINT */
create table admin(
  ID_admin number(11),
  nama_admin varchar2(100) not null,
  username varchar2(20) unique,
  password varchar2(100) not null,
  created_at timestamp,
  constraint pk_id_admin primary key (ID_admin)
);

/* TABLE PARENT */
create table kategori(
  ID_kategori number(11) primary key,
  nama_kategori varchar2(100) not null,
  foto_kategori varchar2(100) not null,
  status_kategori varchar2(20) not null
);

/* TABLE CHILD */
create table produk(
  ID_produk number(11) primary key,
  ID_kategori number(11),
  kode_produk varchar2(50) not null,
  nama_produk varchar2(200) not null,
  deskripsi_produk varchar2(200) not null,
  panjang number(11) not null,
  lebar number(11) not null,
  warna varchar2(100) not null,
  created_at timestamp
);

create sequence seq_id_admin increment by 1;

insert into admin values(
  seq_id_admin.nextval, 'Ahmad', 'Admin', 'admin', sysdate
);

insert into admin values(
  seq_id_admin.nextval, 'Indri', 'Admin2', 'admin2', sysdate
);

insert into t_kategori values /* SALAAAHH */
  (1, 'pakaian', '1.jpg', 'tersedia'),
  (2, 'obat', '2.jpg', 'tersedia'),
  (3, 'lemari', '3.jpg', 'kosong');

insert all
  into t_kategori values (1, 'pakaian', '1.jpg', 'tersedia')
  into t_kategori values (2, 'obat', '2.jpg', 'tersedia')
  into t_kategori values (3, 'lemari', '3.jpg', 'kosong')
  select * from dual;


/* M NAUFAL FAZANADI 1505439