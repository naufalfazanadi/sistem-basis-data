/* =========== BISMILLAH~ ========== */
/* Muhammad Naufal Fazanadi | 1505439 */

/* ---- INIT ---- */
connect system

create user latihan4 identified by latihan4;

grant connect to latihan4;

grant all privileges to latihan4;

connect latihan4;
/* ------------- */

-- WANT TO DROP ? $DROP USER TP1 CASCADE;

create table buku(
  ID_buku varchar2(7) primary key,
  judul_buku varchar2(25) not null,
  pengarang varchar2(15) not null,
  stok NUMBER(12) not null
);

create table anggota_perpus(
  ID_anggota NUMBER(10) primary key,
  nama varchar2(50) not null,
  jk varchar2(2) not null,
  no_telepon varchar2(15) not null
);

create table peminjaman(
  ID_peminjaman NUMBER(10) primary key,
  ID_anggota NUMBER(10) not null,
  ID_buku varchar2(7) not null,
  jumlah_buku NUMBER(10) not null,
  tanggal_peminjaman date not null,
  foreign key (ID_anggota) references anggota_perpus(ID_anggota),
  foreign key (ID_buku) references buku(ID_buku)
);

insert all
  into buku values ('B-01','Basis Data','Fathansyah',20)
  into buku values ('B-02','Algoritma dan Pemograman','Rinaldi Munir',30)
  into buku values ('B-03','Sistem Basis Data','Fathansyah',10)
  select * from dual;

insert all
  into anggota_perpus values (1,'Irma Ayu Aryani','P',085312341234)
  into anggota_perpus values (2,'Yusuf Fakhri','L',085312345678)
  into anggota_perpus values (3,'Wiwi Juwita','P',085312349876)
  select * from dual;

/* TRIGGER */

CREATE OR REPLACE TRIGGER pinjaman
  AFTER
    INSERT OR DELETE OR UPDATE ON peminjaman
  FOR EACH ROW
BEGIN
  IF INSERTING THEN
    UPDATE buku SET stok = stok - :new.jumlah_buku WHERE ID_buku = :new.ID_buku;
  ELSIF DELETING THEN
    UPDATE buku SET stok = stok + :old.jumlah_buku WHERE ID_buku = :old.ID_buku;
  ELSIF UPDATING THEN
    UPDATE buku SET stok = stok + :old.jumlah_buku WHERE ID_buku = :old.ID_buku;
    UPDATE buku SET stok = stok - :new.jumlah_buku WHERE ID_buku = :new.ID_buku;
  END IF;
END;
/

create sequence counter increment by 1;

insert into peminjaman values(counter.nextval, 1, 'B-03', 5, sysdate);
insert into peminjaman values(counter.nextval, 2, 'B-02', 10, sysdate);

create table lomba(
  ID_lomba VARCHAR2(7) primary key,
  nama_lomba VARCHAR2(25) not null,
  jumlah_pendaftar NUMBER(12) not null
);