/* =========== BISMILLAH~ ========== */
/* Muhammad Naufal Fazanadi | 1505439 */

/* ---- INIT ---- */
connect system

create user latihan_trigger identified by latihan_trigger;

grant connect to latihan_trigger;

grant all privileges to latihan_trigger;

connect latihan_trigger;
/* ------------- */

CREATE TABLE lomba(
  ID_lomba VARCHAR2(7) primary key,
  nama_lomba VARCHAR2(25) not null,
  jumlah_pendaftar NUMBER(12) not null
);

CREATE TABLE pendaftaran(
  ID_pendaftaran NUMBER(10) primary key,
  nama VARCHAR2(50) not null,
  ID_lomba VARCHAR2(7) not null,
  jumlah NUMBER(2) not null,
  tanggal DATE not null,
  CONSTRAINT fk_id_lomba FOREIGN KEY (ID_lomba) REFERENCES lomba(ID_lomba) ON DELETE SET NULL
);

/*
SQL> desc lomba;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 ID_LOMBA                                  NOT NULL VARCHAR2(7)
 NAMA_LOMBA                                NOT NULL VARCHAR2(25)
 JUMLAH_PENDAFTAR                          NOT NULL NUMBER(12)

SQL> desc pendaftaran;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 ID_PENDAFTARAN                            NOT NULL NUMBER(10)
 NAMA                                      NOT NULL VARCHAR2(50)
 ID_LOMBA                                  NOT NULL VARCHAR2(7)
 JUMLAH                                    NOT NULL NUMBER(2)
 TANGGAL                                   NOT NULL DATE
 */

INSERT ALL
  INTO lomba VALUES ('DSTAR','Dinamik Star','0')
  INTO lomba VALUES ('OTIK','Olimpiade TIK','0')
  INTO lomba VALUES ('LCW','Lomba Cipta Web','0')
  INTO lomba VALUES ('DRDH','Donor Darah','0')
SELECT * FROM DUAL;

/*
SQL> select * from lomba;

ID_LOMB NAMA_LOMBA                JUMLAH_PENDAFTAR
------- ------------------------- ----------------
DSTAR   Dinamik Star                             0
OTIK    Olimpiade TIK                            0
LCW     Lomba Cipta Web                          0
DRDH    Donor Darah                              0
 */

/* TRIGGER */

CREATE OR REPLACE TRIGGER daftar
  AFTER
    INSERT OR DELETE OR UPDATE ON pendaftaran
  FOR EACH ROW
BEGIN
  IF INSERTING THEN
    UPDATE lomba SET jumlah_pendaftar = jumlah_pendaftar + :new.jumlah WHERE ID_lomba = :new.ID_lomba;
  ELSIF DELETING THEN
    UPDATE lomba SET jumlah_pendaftar = jumlah_pendaftar - :old.jumlah WHERE ID_lomba = :old.ID_lomba;
  ELSIF UPDATING THEN
    UPDATE lomba SET jumlah_pendaftar = jumlah_pendaftar - :old.jumlah WHERE ID_lomba = :old.ID_lomba;
    UPDATE lomba SET jumlah_pendaftar = jumlah_pendaftar + :new.jumlah WHERE ID_lomba = :new.ID_lomba;
  END IF;
END;
/

/* Trigger created. */

INSERT INTO pendaftaran VALUES (24,'deni','DSTAR',2,'14 OCT 2016');

/*
SQL> INSERT INTO pendaftaran VALUES (24,'deni','DSTAR',2,'14 OCT 2016');

1 row created.

SQL> select * from lomba;

ID_LOMB NAMA_LOMBA                JUMLAH_PENDAFTAR
------- ------------------------- ----------------
DSTAR   Dinamik Star                             2
OTIK    Olimpiade TIK                            0
LCW     Lomba Cipta Web                          0
DRDH    Donor Darah                              0

SQL> update pendaftaran set jumlah = 11 where ID_pendaftaran = 24;

1 row updated.

SQL> select * from lomba;

ID_LOMB NAMA_LOMBA                JUMLAH_PENDAFTAR
------- ------------------------- ----------------
DSTAR   Dinamik Star                            11
OTIK    Olimpiade TIK                            0
LCW     Lomba Cipta Web                          0
DRDH    Donor Darah                              0

SQL> delete pendaftaran where ID_pendaftaran = 24;

1 row deleted.

SQL> select * from lomba;

ID_LOMB NAMA_LOMBA                JUMLAH_PENDAFTAR
------- ------------------------- ----------------
DSTAR   Dinamik Star                             0
OTIK    Olimpiade TIK                            0
LCW     Lomba Cipta Web                          0
DRDH    Donor Darah                              0

 */