/* =========== BISMILLAH~ ========== */
/* Muhammad Naufal Fazanadi | 1505439 */

/* ---- INIT ---- */
connect system

create user tp1 identified by tp1;

grant connect to tp1;

grant all privileges to tp1;

connect tp1;
/* ------------- */

-- WANT TO DROP ? $DROP USER TP1 CASCADE;

/* ----------------------------------- NOMOR 1 ----------------------------------------------- */
create table kasir (
  ID_kasir varchar2(10) primary key,
  nama_kasir varchar2(20) not null
);

create table pembeli (
  ID_pembeli varchar2(10) primary key,
  nama_pembeli varchar2(20) not null,
  alamat_pembeli varchar2(20) not null
);

create table barang (
  ID_barang varchar2(10) primary key,
  nama_barang varchar2(20) not null,
  harga_barang number(10) not null,
  stok_barang number(10) not null
);

create table transaksi (
  ID_transaksi number(11) primary key,
  ID_kasir varchar2(10) not null,
  ID_pembeli varchar2(10) not null,
  ID_barang varchar2(10) not null,
  tgl_transaksi timestamp,
  constraint fk_id_kasir foreign key (ID_kasir) references kasir(ID_kasir) ON DELETE CASCADE,
  constraint fk_id_pembeli foreign key (ID_pembeli) references pembeli(ID_pembeli) ON DELETE CASCADE,
  constraint fk_id_barang foreign key (ID_barang) references barang(ID_barang) ON DELETE CASCADE
);

/* ----------------------------------- NOMOR 2 ----------------------------------------------- */
insert all
  into kasir values ('KS01', 'Dendi')
  into kasir values ('KS02', 'Puppey')
  into kasir values ('KS03', 'Fear')
  select * from dual;

insert all
  into pembeli values ('PB001', 'Fata', 'Bandung')
  into pembeli values ('PB002', 'Kuroky', 'Tasikmalaya')
  into pembeli values ('PB003', 'Miracle', 'Subang')
  into pembeli values ('PB004', 'General', 'Bogor')
  into pembeli values ('PB005', 'Sumail', 'Majalengka')
  select * from dual;

insert all
  into barang values ('B001', 'Kaos', 20000, 10)
  into barang values ('B002', 'Celana', 35000, 15)
  into barang values ('B003', 'Jaket', 50000, 8)
  into barang values ('B004', 'Topi', 15000, 20)
  into barang values ('B005', 'Kerudung', 25000, 15)
  into barang values ('B006', 'Kaca Mata', 20000, 30)
  select * from dual;

create sequence counter increment by 1;

insert into transaksi values (counter.nextval, 'KS01', 'PB002', 'B006', sysdate);
insert into transaksi values (counter.nextval, 'KS01', 'PB001', 'B004', sysdate);
insert into transaksi values (counter.nextval, 'KS02', 'PB003', 'B005', sysdate);
insert into transaksi values (counter.nextval, 'KS02', 'PB003', 'B001', sysdate);
insert into transaksi values (counter.nextval, 'KS03', 'PB004', 'B002', sysdate);
insert into transaksi values (counter.nextval, 'KS03', 'PB005', 'B006', sysdate);
insert into transaksi values (counter.nextval, 'KS01', 'PB002', 'B003', sysdate);
insert into transaksi values (counter.nextval, 'KS03', 'PB005', 'B003', sysdate);

/* ----------------------------------- NOMOR 3 ----------------------------------------------- */
select a.nama_pembeli, b.nama_barang, c.tgl_transaksi
from pembeli a, barang b, transaksi c, kasir d
where a.ID_pembeli = c.ID_pembeli and b.ID_barang = c.ID_barang and c.ID_kasir = d.ID_kasir
  and nama_kasir = 'Dendi';

/* ----------------------------------- NOMOR 4 ----------------------------------------------- */
select a.nama_pembeli
from pembeli a, barang b, transaksi c
where a.ID_pembeli = c.ID_pembeli and b.ID_barang = c.ID_barang
  and nama_barang = 'Jaket';

/* ----------------------------------- NOMOR 5 ----------------------------------------------- */
insert into transaksi values (counter.nextval, 'KS03', 'PB005', 'B006', sysdate);
insert into transaksi values (counter.nextval, 'KS02', 'PB003', 'B003', sysdate);
insert into transaksi values (counter.nextval, 'KS01', 'PB001', 'B001', sysdate);

/* ----------------------------------- NOMOR 6 ----------------------------------------------- */
select a.nama_kasir, b.nama_barang
from kasir a, barang b, transaksi c, pembeli d
where a.ID_kasir = c.ID_kasir and b.ID_barang = c.ID_barang and d.ID_pembeli = c.ID_pembeli
  and nama_pembeli like '%ra%';

/* ----------------------------------- NOMOR 7 ----------------------------------------------- */
select a.ID_transaksi, b.ID_kasir, c.nama_pembeli, d.nama_barang, d.stok_barang, a.tgl_transaksi
from transaksi a, kasir b, pembeli c, barang d
where a.ID_kasir = b.ID_kasir and a.ID_pembeli = c.ID_pembeli and a.ID_barang = d.ID_barang
  and nama_kasir = 'Fear'
order by ID_kasir desc;

/* ----------------------------------- NOMOR 8 ----------------------------------------------- */
rename pembeli to pelanggan;

/* ----------------------------------- NOMOR 9 ----------------------------------------------- */
update barang set stok_barang = 50
  where nama_barang = 'Kaos';

update barang set stok_barang = 30
  where nama_barang = 'Kerudung';

/* ---------------------------------- NOMOR 10 ----------------------------------------------- */
alter table barang drop column stok_barang;
alter table pelanggan drop column alamat_pembeli;