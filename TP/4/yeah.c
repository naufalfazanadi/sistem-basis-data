#include <stdio.h>

int main () {
  int n; // jumlah indeks array
  scanf ("%d", &n); // jumlah inputan

  int arr[n]; // array arr sebanyak n indeks

  int i;
  for (i = 0; i < n; i++) {
    scanf("%d", &arr[i]); // input nilai arr indeks ke - i
  }

  for (i = n / 2 - 1; i >= 0; i--) {
    printf("%d\n", arr[i]);
  }

  for (i = n / 2; i < n; i++) {
    printf("%d\n", arr[i]);
  }

  return 0;
}

6
98 56 27 0 23 101

OUTPUT :
27 56 98
0 23 101

9
1 2 3 4 5 6 7 8 9

OUTPUT :
4 3 2 1 5 6 7 8 9

6
1 2 3 4 5 6

OUTPUT :
4 5 6 1 2 3

10
1 2 3 4 5 6 7 8 9 10

OUTPUT :
6 7 8 9 10 1 2 3 4 5