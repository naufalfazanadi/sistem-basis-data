/* =========== BISMILLAH~ ========== */
/* Muhammad Naufal Fazanadi | 1505439 */

/* ---- INIT ---- */
connect system

create user tp4 identified by tp4;

grant connect to tp4;

grant all privileges to tp4;

connect tp4;
/* ------------- */

CREATE SEQUENCE counter_salesman INCREMENT BY 1;
CREATE SEQUENCE counter_barang INCREMENT BY 1;
CREATE SEQUENCE counter_transaksi INCREMENT BY 1;

/* ------------------------- NOMOR 1 ------------------------- */
CREATE TABLE salesman(
  ID_salesman NUMBER primary key,
  nama_salesman VARCHAR2(25) not null,
  poin_harian NUMBER not null,
  tanggal_aktif TIMESTAMP,
  tanggal_pasif TIMESTAMP
);
/*
SQL> desc salesman;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 ID_SALESMAN                               NOT NULL NUMBER
 NAMA_SALESMAN                             NOT NULL VARCHAR2(25)
 POIN_HARIAN                               NOT NULL NUMBER
 TANGGAL_AKTIF                                      TIMESTAMP(6)
 TANGGAL_PASIF                                      TIMESTAMP(6)

*/

/* ------------------------- NOMOR 2 ------------------------- */
CREATE TABLE barang(
  ID_barang NUMBER primary key,
  nama_barang VARCHAR2(25) not null,
  jenis_barang VARCHAR2(25) not null,
  harga_barang NUMBER not null,
  stok_barang NUMBER not null
);

INSERT INTO barang VALUES (counter_barang.nextval,'ASUS','Laptop',5000000,6);
INSERT INTO barang VALUES (counter_barang.nextval,'Logitech','Mouse',500000,15);
INSERT INTO barang VALUES (counter_barang.nextval,'Acer','Laptop',3000000,5);
INSERT INTO barang VALUES (counter_barang.nextval,'NVIDIA','VGA',2000000,10);

/*
SQL> desc barang
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 ID_BARANG                                 NOT NULL NUMBER
 NAMA_BARANG                               NOT NULL VARCHAR2(25)
 JENIS_BARANG                              NOT NULL VARCHAR2(25)
 HARGA_BARANG                              NOT NULL NUMBER
 STOK_BARANG                               NOT NULL NUMBER

SQL> select * from barang;

 ID_BARANG NAMA_BARANG               JENIS_BARANG              HARGA_BARANG
---------- ------------------------- ------------------------- ------------
STOK_BARANG
-----------
         1 ASUS                      Laptop                         5000000
          6

         2 Logitech                  Mouse                           500000
         15

         3 Acer                      Laptop                         3000000
          5


 ID_BARANG NAMA_BARANG               JENIS_BARANG              HARGA_BARANG
---------- ------------------------- ------------------------- ------------
STOK_BARANG
-----------
         4 NVIDIA                    VGA                            2000000
         10

 */

/* ------------------------- NOMOR 3 ------------------------- */
CREATE TABLE transaksi(
  ID_transaksi NUMBER primary key,
  ID_salesman NUMBER not null,
  ID_barang NUMBER not null,
  jumlah_terjual NUMBER not null,
  total_transaksi NUMBER,
  CONSTRAINT fk_id_salesman FOREIGN KEY (ID_salesman) REFERENCES salesman(ID_salesman) ON DELETE SET NULL,
  CONSTRAINT fk_id_barang FOREIGN KEY (ID_barang) REFERENCES barang(ID_barang) ON DELETE SET NULL
);

/*
SQL> desc transaksi
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 ID_TRANSAKSI                              NOT NULL NUMBER
 ID_SALESMAN                               NOT NULL NUMBER
 ID_BARANG                                 NOT NULL NUMBER
 JUMLAH_TERJUAL                            NOT NULL NUMBER
 TOTAL_TRANSAKSI                                    NUMBER
 */

/* ------------------------- NOMOR 4 ------------------------- */
CREATE OR REPLACE TRIGGER tanggal
  BEFORE
    INSERT ON salesman
  FOR EACH ROW
BEGIN
  IF INSERTING THEN
    :new.tanggal_aktif := SYSDATE;
    :new.tanggal_pasif := ADD_MONTHS(SYSDATE, 12);
  END IF;
END;
/

INSERT INTO salesman VALUES (counter_salesman.nextval,'Naufal',0,NULL,NULL);
INSERT INTO salesman VALUES (counter_salesman.nextval,'Ammar',0,NULL,NULL);
INSERT INTO salesman VALUES (counter_salesman.nextval,'Achmad',0,NULL,NULL);
INSERT INTO salesman VALUES (counter_salesman.nextval,'Herlina',0,NULL,NULL);
INSERT INTO salesman VALUES (counter_salesman.nextval,'Yuni',0,NULL,NULL);

/*
SQL> select * from salesman;

ID_SALESMAN NAMA_SALESMAN             POIN_HARIAN
----------- ------------------------- -----------
TANGGAL_AKTIF
---------------------------------------------------------------------------
TANGGAL_PASIF
---------------------------------------------------------------------------
          1 Naufal                              0
23-OCT-16 10.48.43.000000 PM
23-OCT-17 10.48.43.000000 PM

          2 Ammar                               0
23-OCT-16 10.49.08.000000 PM
23-OCT-17 10.49.08.000000 PM

ID_SALESMAN NAMA_SALESMAN             POIN_HARIAN
----------- ------------------------- -----------
TANGGAL_AKTIF
---------------------------------------------------------------------------
TANGGAL_PASIF
---------------------------------------------------------------------------

          3 Achmad                              0
23-OCT-16 10.49.08.000000 PM
23-OCT-17 10.49.08.000000 PM

          4 Herlina                             0
23-OCT-16 10.49.08.000000 PM

ID_SALESMAN NAMA_SALESMAN             POIN_HARIAN
----------- ------------------------- -----------
TANGGAL_AKTIF
---------------------------------------------------------------------------
TANGGAL_PASIF
---------------------------------------------------------------------------
23-OCT-17 10.49.08.000000 PM

          5 Yuni                                0
23-OCT-16 10.49.08.000000 PM
23-OCT-17 10.49.08.000000 PM

 */

/* ------------------------- NOMOR 5 ------------------------- */
CREATE OR REPLACE FUNCTION loc_harga (var_id IN VARCHAR2) RETURN NUMBER AS
  var_result NUMBER;
BEGIN
  SELECT harga_barang INTO var_result FROM barang WHERE ID_barang = var_id;
  RETURN (var_result);
END loc_harga;
/

CREATE OR REPLACE TRIGGER totalTrans
  BEFORE
    INSERT OR DELETE OR UPDATE ON transaksi
  FOR EACH ROW
BEGIN
  IF INSERTING THEN
    :new.total_transaksi := loc_harga(:new.ID_barang) * :new.jumlah_terjual;
    UPDATE barang SET stok_barang = stok_barang - :new.jumlah_terjual WHERE ID_barang = :new.ID_barang;
  ELSIF DELETING THEN
    UPDATE barang SET stok_barang = stok_barang + :old.jumlah_terjual WHERE ID_barang = :old.ID_barang;
  ELSIF UPDATING THEN
    :new.total_transaksi := loc_harga(:new.ID_barang) * :new.jumlah_terjual;
    UPDATE barang SET stok_barang = stok_barang + :old.jumlah_terjual WHERE ID_barang = :old.ID_barang;
    UPDATE barang SET stok_barang = stok_barang - :new.jumlah_terjual WHERE ID_barang = :new.ID_barang;
  END IF;
END;
/



/* ------------------------- NOMOR 6 ------------------------- */
CREATE OR REPLACE TRIGGER poin
  AFTER
    INSERT OR DELETE OR UPDATE ON transaksi
  FOR EACH ROW
BEGIN
  IF INSERTING THEN
    IF :new.total_transaksi < 300000 THEN
      UPDATE salesman SET poin_harian = poin_harian + 10 WHERE ID_salesman = :new.ID_salesman;
    ELSIF (:new.total_transaksi < 350000) AND (:new.total_transaksi >= 300000) THEN
      UPDATE salesman SET poin_harian = poin_harian + 15 WHERE ID_salesman = :new.ID_salesman;
    ELSIF (:new.total_transaksi < 400000) AND (:new.total_transaksi >= 350000) THEN
      UPDATE salesman SET poin_harian = poin_harian + 20 WHERE ID_salesman = :new.ID_salesman;
    ELSIF (:new.total_transaksi < 450000) AND (:new.total_transaksi >= 400000) THEN
      UPDATE salesman SET poin_harian = poin_harian + 30 WHERE ID_salesman = :new.ID_salesman;
    ELSIF (:new.total_transaksi < 500000) AND (:new.total_transaksi >= 450000) THEN
      UPDATE salesman SET poin_harian = poin_harian + 40 WHERE ID_salesman = :new.ID_salesman;
    ELSIF (:new.total_transaksi < 1000000) AND (:new.total_transaksi >= 500000) THEN
      UPDATE salesman SET poin_harian = poin_harian + 50 WHERE ID_salesman = :new.ID_salesman;
    ELSIF (:new.total_transaksi < 2000000) AND (:new.total_transaksi >= 1000000) THEN
      UPDATE salesman SET poin_harian = poin_harian + 100 WHERE ID_salesman = :new.ID_salesman;
    ELSE /* OPTIONAL */
      UPDATE salesman SET poin_harian = poin_harian + 200 WHERE ID_salesman = :new.ID_salesman;
    END IF;
  ELSIF DELETING THEN
    IF :old.total_transaksi < 300000 THEN
      UPDATE salesman SET poin_harian = poin_harian - 10 WHERE ID_salesman = :old.ID_salesman;
    ELSIF :old.total_transaksi < 350000 THEN
      UPDATE salesman SET poin_harian = poin_harian - 15 WHERE ID_salesman = :old.ID_salesman;
    ELSIF :old.total_transaksi < 400000 THEN
      UPDATE salesman SET poin_harian = poin_harian - 20 WHERE ID_salesman = :old.ID_salesman;
    ELSIF :old.total_transaksi < 450000 THEN
      UPDATE salesman SET poin_harian = poin_harian - 30 WHERE ID_salesman = :old.ID_salesman;
    ELSIF :old.total_transaksi < 500000 THEN
      UPDATE salesman SET poin_harian = poin_harian - 40 WHERE ID_salesman = :old.ID_salesman;
    ELSIF :old.total_transaksi < 1000000 THEN
      UPDATE salesman SET poin_harian = poin_harian - 50 WHERE ID_salesman = :old.ID_salesman;
    ELSIF :old.total_transaksi < 2000000 THEN
      UPDATE salesman SET poin_harian = poin_harian - 100 WHERE ID_salesman = :old.ID_salesman;
    END IF;
  ELSIF UPDATING THEN
    /*KEMBALIKAN DULU*/
    IF :old.total_transaksi < 300000 THEN
      UPDATE salesman SET poin_harian = poin_harian - 10 WHERE ID_salesman = :old.ID_salesman;
    ELSIF :old.total_transaksi < 350000 THEN
      UPDATE salesman SET poin_harian = poin_harian - 15 WHERE ID_salesman = :old.ID_salesman;
    ELSIF :old.total_transaksi < 400000 THEN
      UPDATE salesman SET poin_harian = poin_harian - 20 WHERE ID_salesman = :old.ID_salesman;
    ELSIF :old.total_transaksi < 450000 THEN
      UPDATE salesman SET poin_harian = poin_harian - 30 WHERE ID_salesman = :old.ID_salesman;
    ELSIF :old.total_transaksi < 500000 THEN
      UPDATE salesman SET poin_harian = poin_harian - 40 WHERE ID_salesman = :old.ID_salesman;
    ELSIF :old.total_transaksi < 1000000 THEN
      UPDATE salesman SET poin_harian = poin_harian - 50 WHERE ID_salesman = :old.ID_salesman;
    ELSIF :old.total_transaksi < 2000000 THEN
      UPDATE salesman SET poin_harian = poin_harian - 100 WHERE ID_salesman = :old.ID_salesman;
    END IF;
    /*ADD OLEH YG BARU*/
    IF :new.total_transaksi < 300000 THEN
      UPDATE salesman SET poin_harian = poin_harian + 10 WHERE ID_salesman = :new.ID_salesman;
    ELSIF :new.total_transaksi < 350000 THEN
      UPDATE salesman SET poin_harian = poin_harian + 15 WHERE ID_salesman = :new.ID_salesman;
    ELSIF :new.total_transaksi < 400000 THEN
      UPDATE salesman SET poin_harian = poin_harian + 20 WHERE ID_salesman = :new.ID_salesman;
    ELSIF :new.total_transaksi < 450000 THEN
      UPDATE salesman SET poin_harian = poin_harian + 30 WHERE ID_salesman = :new.ID_salesman;
    ELSIF :new.total_transaksi < 500000 THEN
      UPDATE salesman SET poin_harian = poin_harian + 40 WHERE ID_salesman = :new.ID_salesman;
    ELSIF :new.total_transaksi < 1000000 THEN
      UPDATE salesman SET poin_harian = poin_harian + 50 WHERE ID_salesman = :new.ID_salesman;
    ELSIF :new.total_transaksi < 2000000 THEN
      UPDATE salesman SET poin_harian = poin_harian + 100 WHERE ID_salesman = :new.ID_salesman;
    END IF;
  END IF;
END;
/


/* NOMOR 7 */

INSERT INTO transaksi VALUES (counter_transaksi.nextval,1,1,4,null);
INSERT INTO transaksi VALUES (counter_transaksi.nextval,2,2,5,null);
INSERT INTO transaksi VALUES (counter_transaksi.nextval,3,3,3,null);
INSERT INTO transaksi VALUES (counter_transaksi.nextval,4,4,4,null);
INSERT INTO transaksi VALUES (counter_transaksi.nextval,5,4,5,null);

-- CHECK
UPDATE transaksi SET jumlah_terjual = 1 where ID_transaksi = 1;
/*
SQL> select * from transaksi;

ID_TRANSAKSI ID_SALESMAN  ID_BARANG JUMLAH_TERJUAL TOTAL_TRANSAKSI
------------ ----------- ---------- -------------- ---------------
           1           1          1              4        20000000
           2           2          2              5         2500000
           3           3          3              3         9000000
           4           4          4              4         8000000
           5           5          4              5        10000000

SQL> select * from barang;

 ID_BARANG NAMA_BARANG               JENIS_BARANG              HARGA_BARANG
---------- ------------------------- ------------------------- ------------
STOK_BARANG
-----------
         1 ASUS                      Laptop                         5000000
          2

         2 Logitech                  Mouse                           500000
         10

         3 Acer                      Laptop                         3000000
          2


 ID_BARANG NAMA_BARANG               JENIS_BARANG              HARGA_BARANG
---------- ------------------------- ------------------------- ------------
STOK_BARANG
-----------
         4 NVIDIA                    VGA                            2000000
          1

SQL> UPDATE transaksi SET jumlah_terjual = 1 where ID_transaksi = 1;

1 row updated.

SQL> select * from transaksi;

ID_TRANSAKSI ID_SALESMAN  ID_BARANG JUMLAH_TERJUAL TOTAL_TRANSAKSI
------------ ----------- ---------- -------------- ---------------
           1           1          1              1         5000000
           2           2          2              5         2500000
           3           3          3              3         9000000
           4           4          4              4         8000000
           5           5          4              5        10000000

SQL> select * from barang;

 ID_BARANG NAMA_BARANG               JENIS_BARANG              HARGA_BARANG
---------- ------------------------- ------------------------- ------------
STOK_BARANG
-----------
         1 ASUS                      Laptop                         5000000
          5

         2 Logitech                  Mouse                           500000
         10

         3 Acer                      Laptop                         3000000
          2


 ID_BARANG NAMA_BARANG               JENIS_BARANG              HARGA_BARANG
---------- ------------------------- ------------------------- ------------
STOK_BARANG
-----------
         4 NVIDIA                    VGA                            2000000
          1

/*
SQL> select * from salesman;

ID_SALESMAN NAMA_SALESMAN             POIN_HARIAN
----------- ------------------------- -----------
TANGGAL_AKTIF
---------------------------------------------------------------------------
TANGGAL_PASIF
---------------------------------------------------------------------------
          1 Naufal                            200
23-OCT-16 11.41.47.000000 PM
23-OCT-17 11.41.47.000000 PM

          2 Ammar                             200
23-OCT-16 11.41.47.000000 PM
23-OCT-17 11.41.47.000000 PM

ID_SALESMAN NAMA_SALESMAN             POIN_HARIAN
----------- ------------------------- -----------
TANGGAL_AKTIF
---------------------------------------------------------------------------
TANGGAL_PASIF
---------------------------------------------------------------------------

          3 Achmad                            200
23-OCT-16 11.41.47.000000 PM
23-OCT-17 11.41.47.000000 PM

          4 Herlina                           200
23-OCT-16 11.41.47.000000 PM

ID_SALESMAN NAMA_SALESMAN             POIN_HARIAN
----------- ------------------------- -----------
TANGGAL_AKTIF
---------------------------------------------------------------------------
TANGGAL_PASIF
---------------------------------------------------------------------------
23-OCT-17 11.41.47.000000 PM

          5 Yuni                              200
23-OCT-16 11.41.47.000000 PM
23-OCT-17 11.41.47.000000 PM


 */
