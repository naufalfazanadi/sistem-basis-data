/* =========== BISMILLAH~ ========== */
/* Muhammad Naufal Fazanadi | 1505439 */

/* ---- INIT ---- */
connect system;

create user perpustakaan identified by perpustakaan;

grant connect to perpustakaan;

grant all privileges to perpustakaan;

connect perpustakaan;

set colsep '|'
set linesize 167
set pagesize 30
set pagesize 1000

TRUNCATE TABLE ...; -- clear data

/* ---------- */

-- WANT TO DROP ? $DROP USER ... CASCADE;

/* ----------------------------------- NOMOR 1 ----------------------------------------------- */
create table t_petugas_perpus (
  ID_petugas varchar2(10) primary key,
  nama_petugas varchar2(16) not null,
  alamat_petugas varchar2(32) not null
);

create table t_peminjam (
  ID_peminjam varchar2(10) primary key,
  nama_peminjam varchar2(16) not null,
  jurusan varchar2(16) not null,
  instansi_peminjam varchar2(32) not null
);

create table t_buku (
  KD_buku varchar2(10) primary key,
  judul varchar2(32) not null,
  pengarang varchar2(16) not null,
  tahun number not null,
  stok_buku number not null
);

create table t_peminjaman (
  ID_peminjaman number primary key,
  ID_peminjam varchar2(10) not null,
  ID_petugas varchar2(10) not null,
  KD_buku varchar2(10) not null,
  jumlah_buku number not null,
  tgl_pinjam timestamp not null,
  constraint fk_id_peminjam foreign key (ID_peminjam) references t_peminjam(ID_peminjam) ON DELETE CASCADE,
  constraint fk_id_petugas foreign key (ID_petugas) references t_petugas_perpus(ID_petugas) ON DELETE CASCADE,
  constraint fk_kd_buku foreign key (KD_buku) references t_buku(KD_buku) ON DELETE CASCADE
);

/* ----------------------------------- NOMOR 2 ----------------------------------------------- */
insert all
  into t_petugas_perpus values ('PET-01','Lala','Jl. Cinta')
  into t_petugas_perpus values ('PET-02','Lili','Jl. Masa Lalu')
  into t_petugas_perpus values ('PET-03','Lulu','Jl. Kenangan')
  into t_petugas_perpus values ('PET-04','Lolo','Jl. Yang diridhoi')
  select * from dual;

insert all
  into t_peminjam values ('PEM-01','Wina','Ilmu Komputer','MIT')
  into t_peminjam values ('PEM-02','Yusuf','Ilmu Komputer','Oxford University')
  into t_peminjam values ('PEM-03','Acep','Ilmu Komputer','Harvard University')
  into t_peminjam values ('PEM-04','Yusup','P. Ilmu Komputer','Cambridge University')
  select * from dual;

insert all
  into t_buku values ('BUK-01','Saya Suka Dia','Cukro',2016,55)
  into t_buku values ('BUK-02','Kamu Suka Aku','Cukra',2015,67)
  into t_buku values ('BUK-03','Kita Sama Sama Suka','Robin',2010,73)
  into t_buku values ('BUK-04','Jangan Ada Orang Ketiga','Hudin',2009,50)
  into t_buku values ('BUK-05','Diantara Kita','Jeremy',2014,95)
  select * from dual;

/* ----------------------------------- NOMOR 3 ----------------------------------------------- */
DECLARE
  id VARCHAR2(10);
  nama VARCHAR2(16);
  alamat VARCHAR2(32);
BEGIN
  id := 'PET-05';
  nama := 'Nama Kamu...';
  alamat := 'Alamat kamu..iya kamu....';

  INSERT INTO t_petugas_perpus VALUES (id, nama, alamat);
END;
/

/* ----------------------------------- NOMOR 4 ----------------------------------------------- */
create sequence counter increment by 1;
/* DROP SEQUENCE counter; */

CREATE OR REPLACE PROCEDURE prc_tambah_peminjaman
  (ID_peminjam IN VARCHAR2, ID_petugas IN VARCHAR2, KD_buku_in IN VARCHAR2, jumlah_buku IN NUMBER, tgl_pinjam IN timestamp) AS

BEGIN
  INSERT INTO t_peminjaman VALUES (counter.nextval, ID_peminjam, ID_petugas, KD_buku_in, jumlah_buku, tgl_pinjam);
  UPDATE t_buku SET stok_buku = (stok_buku - jumlah_buku) WHERE KD_buku = KD_buku_in;
END prc_tambah_peminjaman;
/

exec prc_tambah_peminjaman('PEM-01','PET-01','BUK-02',5,'02-oct-2016 07.50.37');
exec prc_tambah_peminjaman('PEM-01','PET-02','BUK-03',9,'02-oct-16 07.51.16');
exec prc_tambah_peminjaman('PEM-02','PET-03','BUK-01',3,'02-oct-16 07.51.40');
exec prc_tambah_peminjaman('PEM-03','PET-04','BUK-05',10,'02-oct-16 07.52.08');
exec prc_tambah_peminjaman('PEM-04','PET-01','BUK-04',7,'02-oct-16 07.52.55');
exec prc_tambah_peminjaman('PEM-03','PET-04','BUK-04',5,'02-oct-16 07.53.21');

/* ----------------------------------- NOMOR 5 ----------------------------------------------- */
CREATE OR REPLACE PROCEDURE prc_tambah_stok_buku (KD_buku_in IN VARCHAR2, stok_buku_in IN NUMBER) AS

BEGIN
  UPDATE t_buku SET stok_buku = (stok_buku + stok_buku_in) WHERE KD_buku = KD_buku_in;
END prc_tambah_stok_buku;
/

-- A
exec prc_tambah_stok_buku('BUK-01',15);

-- B
exec prc_tambah_stok_buku('BUK-04',20);

-- C
exec prc_tambah_stok_buku('BUK-03',16);
