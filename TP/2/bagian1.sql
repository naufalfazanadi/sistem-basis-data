/* =========== BISMILLAH~ ========== */
/* Muhammad Naufal Fazanadi | 1505439 */

/* ---- INIT ---- */
connect system;

create user tp2 identified by tp2;

grant connect to tp2;

grant all privileges to tp2;

connect tp2;

set colsep '|'
set linesize 167
set pagesize 30
set pagesize 1000
/* ---------- */

set serveroutput on;

/* TABLE */

create table t_faktorial (
  hasil NUMBER(4)
);

TRUNCATE TABLE t_faktorial; -- clear data

/* IN AND OUT */
CREATE OR REPLACE PROCEDURE faktorial (n IN NUMBER, hasil OUT NUMBER) AS
  v_counter NUMBER;
BEGIN
  v_counter := 1;
  hasil := v_counter;

  LOOP
    hasil := hasil * v_counter;

    INSERT INTO t_faktorial(hasil) VALUES (hasil);

    v_counter := v_counter + 1;

    EXIT WHEN v_counter > n;
  END LOOP;
END faktorial;
/

/* ANONYMOUSBLOCK */
DECLARE
  n NUMBER;
  i NUMBER;
  j NUMBER;
  v_output NUMBER;
BEGIN
  n := 6; -- <- jumlah faktorial (bisa diedit)
  faktorial(n, v_output);

  DBMS_OUTPUT.PUT_LINE(n || '! = ' || v_output);

  i := n;
  FOR i IN REVERSE 0..n LOOP
    j := 0;
    FOR j IN 0..i-1 LOOP
      IF j = i THEN
        DBMS_OUTPUT.PUT(v_output);
      ELSE
        DBMS_OUTPUT.PUT(v_output || ' ');
      END IF;
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('');
  END LOOP;

END;
/