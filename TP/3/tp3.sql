/* =========== BISMILLAH~ ========== */
/* Muhammad Naufal Fazanadi | 1505439 */

/* ---- INIT ---- */
connect system

create user tp3 identified by tp3;

grant connect to tp3;

grant all privileges to tp3;

connect tp3;
/* ------------- */

/*
SQL> create user tp3 identified by tp3;

User created.

SQL>
SQL> grant connect to tp3;

Grant succeeded.

SQL>
SQL> grant all privileges to tp3;

Grant succeeded.

SQL>
SQL> connect tp3;
Enter password:
Connected.
 */

-- WANT TO DROP ? $DROP USER ... CASCADE;
-- TRUNCATE TABLE ...; -- clear data
-- delete from TABLE where CONDITION;

/* ------------------------- NOMOR 1 ------------------------- */
CREATE TABLE barang(
  ID_barang VARCHAR2(5) primary key,
  nama_barang VARCHAR2(30) not null,
  harga NUMBER(12) not null
);

CREATE TABLE pembeli(
  ID_pembeli VARCHAR2(5) primary key,
  nama_pembeli VARCHAR2(30) not null,
  telp_pembeli VARCHAR2(13) not null
);

CREATE TABLE kasir(
  ID_kasir VARCHAR2(5) primary key,
  nama_kasir VARCHAR2(30) not null,
  telp_kasir VARCHAR2(13) not null
);

CREATE TABLE transaksi(
  ID_transaksi NUMBER(3) primary key,
  ID_pembeli VARCHAR2(5) not null,
  ID_kasir VARCHAR2(5) not null,
  ID_barang VARCHAR2(5) not null,
  jumlah_beli NUMBER(3) not null,
  total_transaksi NUMBER(15),
  constraint fk_id_pembeli foreign key (ID_pembeli) references pembeli(ID_pembeli) on delete set null,
  constraint fk_id_kasir foreign key (ID_kasir) references kasir(ID_kasir) on delete set null,
  constraint fk_id_barang foreign key (ID_barang) references barang(ID_barang) on delete set null
);

CREATE TABLE bonus(
  no_bonus NUMBER(3) primary key,
  ID_pembeli VARCHAR2(5) not null,
  bonus VARCHAR2(15) not null,
  constraint fk_id_pembeli_bonus foreign key (ID_pembeli) references pembeli(ID_pembeli) on delete set null
);

/*
SQL> desc barang;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 ID_BARANG                                 NOT NULL VARCHAR2(5)
 NAMA_BARANG                               NOT NULL VARCHAR2(30)
 HARGA                                     NOT NULL NUMBER(12)

SQL> desc pembeli;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 ID_PEMBELI                                NOT NULL VARCHAR2(5)
 NAMA_PEMBELI                              NOT NULL VARCHAR2(30)
 TELP_PEMBELI                              NOT NULL VARCHAR2(13)

SQL> desc kasir;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 ID_KASIR                                  NOT NULL VARCHAR2(5)
 NAMA_KASIR                                NOT NULL VARCHAR2(30)
 TELP_KASIR                                NOT NULL VARCHAR2(13)

SQL> desc transaksi;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 ID_TRANSAKSI                              NOT NULL NUMBER(3)
 ID_PEMBELI                                NOT NULL VARCHAR2(5)
 ID_KASIR                                  NOT NULL VARCHAR2(5)
 ID_BARANG                                 NOT NULL VARCHAR2(5)
 JUMLAH_BELI                               NOT NULL NUMBER(3)
 TOTAL_TRANSAKSI                                    NUMBER(15)

SQL> desc bonus;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 NO_BONUS                                  NOT NULL NUMBER(3)
 ID_PEMBELI                                NOT NULL VARCHAR2(5)
 BONUS                                     NOT NULL VARCHAR2(15)

 */

/* ------------------------- NOMOR 2 ------------------------- */
INSERT ALL
  INTO barang VALUES ('B001','Power Bank',250000)
  INTO barang VALUES ('B002','Headphone',150000)
  INTO barang VALUES ('B003','Gorilla Glass',100000)
  INTO barang VALUES ('B004','Kabel data',10000)
  INTO barang VALUES ('B005','Tongsis',30000)
SELECT * FROM DUAL;

INSERT ALL
  INTO pembeli VALUES ('P001','Anton','081222333111')
  INTO pembeli VALUES ('P002','Intan','081222333222')
  INTO pembeli VALUES ('P003','Reno','081222333333')
SELECT * FROM DUAL;

INSERT ALL
  INTO kasir VALUES ('K001','Rina','082222333111')
  INTO kasir VALUES ('K002','Rinton','082222333111')
SELECT * FROM DUAL;

CREATE SEQUENCE counter INCREMENT BY 1;

INSERT INTO transaksi VALUES (counter.nextval,'P001','K001','B001',3,null);
INSERT INTO transaksi VALUES (counter.nextval,'P002','K002','B002',2,null);
INSERT INTO transaksi VALUES (counter.nextval,'P003','K001','B003',5,null);
INSERT INTO transaksi VALUES (counter.nextval,'P001','K002','B004',1,null);
INSERT INTO transaksi VALUES (counter.nextval,'P002','K001','B005',2,null);
INSERT INTO transaksi VALUES (counter.nextval,'P003','K002','B001',4,null);
INSERT INTO transaksi VALUES (counter.nextval,'P001','K001','B002',2,null);

/*
SQL> select * from barang;

ID_BA NAMA_BARANG                         HARGA
----- ------------------------------ ----------
B001  Power Bank                         250000
B002  Headphone                          150000
B003  Gorilla Glass                      100000
B004  Kabel data                          10000
B005  Tongsis                             30000

SQL> select * from pembeli;

ID_PE NAMA_PEMBELI                   TELP_PEMBELI
----- ------------------------------ -------------
P001  Anton                          081222333111
P002  Intan                          081222333222
P003  Reno                           081222333333

SQL> select * from kasir;

ID_KA NAMA_KASIR                     TELP_KASIR
----- ------------------------------ -------------
K001  Rina                           082222333111
K002  Rinton                         082222333111

SQL> select * from transaksi;

ID_TRANSAKSI ID_PE ID_KA ID_BA JUMLAH_BELI TOTAL_TRANSAKSI
------------ ----- ----- ----- ----------- ---------------
           1 P001  K001  B001            3
           2 P002  K002  B002            2
           3 P003  K001  B003            5
           4 P001  K002  B004            1
           5 P002  K001  B005            2
           6 P003  K002  B001            4
           7 P001  K001  B002            2
 */

/* ------------------------- NOMOR 3 ------------------------- */
CREATE OR REPLACE FUNCTION min_function RETURN NUMBER AS
  var_min NUMBER;
BEGIN
  SELECT MIN(harga) INTO var_min FROM barang;
  RETURN (var_min);
END min_function;
/

SELECT min_function FROM DUAL;

/*
SQL> SELECT min_function FROM DUAL;

MIN_FUNCTION
------------
       10000
 */

/* ------------------------- NOMOR 4 ------------------------- */
SELECT nama_barang, min_function FROM barang;

/*
SQL> SELECT nama_barang, min_function FROM barang;

NAMA_BARANG                    MIN_FUNCTION
------------------------------ ------------
Power Bank                            10000
Headphone                             10000
Gorilla Glass                         10000
Kabel data                            10000
Tongsis                               10000
 */

/* ------------------------- NOMOR 5 ------------------------- */
CREATE OR REPLACE FUNCTION l_function RETURN NUMBER AS
  c_harga barang.harga%type;
BEGIN
  SELECT harga INTO c_harga FROM barang WHERE harga > 180000;
  RETURN (c_harga);
END l_function;
/

SELECT l_function FROM DUAL;

/*
SQL> SELECT l_function FROM DUAL;

L_FUNCTION
----------
    250000
 */

/* ------------------------- NOMOR 6 ------------------------- */
SET SERVEROUTPUT ON;

DECLARE
  var_id transaksi.ID_transaksi%type;
  var_jumlah transaksi.jumlah_beli%type;
  CURSOR c_id IS SELECT ID_transaksi FROM transaksi;
BEGIN
  OPEN c_id;
  LOOP
    FETCH c_id INTO var_id;
    EXIT WHEN c_id%notfound;

      SELECT jumlah_beli INTO var_jumlah FROM transaksi WHERE ID_transaksi = var_id;

      IF var_jumlah > 3 THEN
        DBMS_OUTPUT.PUT_LINE(var_id);
      END IF;

  END LOOP;
  CLOSE c_id;
END;
/

/*
SQL> SET SERVEROUTPUT ON;
SQL> DECLARE
  2    var_id transaksi.ID_transaksi%type;
  3    var_jumlah transaksi.jumlah_beli%type;
  4    CURSOR c_id IS SELECT ID_transaksi FROM transaksi;
  5  BEGIN
  6    OPEN c_id;
  7    LOOP
  8      FETCH c_id INTO var_id;
  9      EXIT WHEN c_id%notfound;
 10
 11        SELECT jumlah_beli INTO var_jumlah FROM transaksi WHERE ID_transaksi = var_id;
 12
 13        IF var_jumlah > 3 THEN
 14          DBMS_OUTPUT.PUT_LINE(var_id);
 15        END IF;
 16
 17    END LOOP;
 18    CLOSE c_id;
 19  END;
 20  /
3
6

PL/SQL procedure successfully completed.
 */

/* ------------------------- NOMOR 7 & 8 ------------------------- */
CREATE OR REPLACE PROCEDURE prc_total AS
  var_id transaksi.ID_transaksi%type;
  var_idbarang transaksi.ID_barang%type;
  var_harga barang.harga%type;
  CURSOR c_id IS SELECT ID_transaksi FROM transaksi;
BEGIN
  OPEN c_id;
  LOOP
    FETCH c_id INTO var_id;
    EXIT WHEN c_id%notfound;

      SELECT ID_barang INTO var_idbarang FROM transaksi WHERE ID_transaksi = var_id;
      SELECT harga INTO var_harga FROM barang WHERE ID_barang = var_idbarang;

      UPDATE transaksi SET total_transaksi = (var_harga * jumlah_beli) WHERE ID_transaksi = var_id;

  END LOOP;
  CLOSE c_id;
END prc_total;
/

EXEC prc_total;

SELECT * FROM transaksi;

/*
SQL> select * from transaksi;

ID_TRANSAKSI ID_PE ID_KA ID_BA JUMLAH_BELI TOTAL_TRANSAKSI
------------ ----- ----- ----- ----------- ---------------
           1 P001  K001  B001            3          750000
           2 P002  K002  B002            2          300000
           3 P003  K001  B003            5          500000
           4 P001  K002  B004            1           10000
           5 P002  K001  B005            2           60000
           6 P003  K002  B001            4         1000000
           7 P001  K001  B002            2          300000

 */

/* ------------------------- NOMOR 9 & 10 ------------------------- */
CREATE SEQUENCE counter_bonus INCREMENT BY 1;

CREATE OR REPLACE FUNCTION hitung_transaksi (var_id IN VARCHAR2) RETURN NUMBER AS
  var_result NUMBER;
BEGIN
  SELECT COUNT(ID_pembeli) INTO var_result FROM transaksi WHERE ID_pembeli = var_id;
  RETURN (var_result);
END hitung_transaksi;
/

CREATE OR REPLACE PROCEDURE prc_isi_bonus AS
  var_id transaksi.ID_pembeli%type;
  var_jumlah NUMBER;
  CURSOR c_id IS SELECT ID_pembeli FROM transaksi GROUP BY ID_pembeli ORDER BY ID_pembeli ASC;
BEGIN
  OPEN c_id;
  LOOP
    FETCH c_id INTO var_id;
    EXIT WHEN c_id%notfound;

      SELECT hitung_transaksi(var_id) INTO var_jumlah FROM DUAL;

      IF var_jumlah = 2 THEN
        INSERT INTO bonus VALUES (counter_bonus.nextval, var_id, 'Gelas Ganteng');
      ELSIF var_jumlah > 2 THEN
        INSERT INTO bonus VALUES (counter_bonus.nextval, var_id, 'Piring cantik');
      END IF;

  END LOOP;
  CLOSE c_id;
END prc_isi_bonus;
/

EXEC prc_isi_bonus;

SELECT * FROM bonus;

/*
SQL> SELECT * FROM bonus;

  NO_BONUS ID_PE BONUS
---------- ----- ---------------
         1 P001  Piring cantik
         2 P002  Gelas Ganteng
         3 P003  Gelas Ganteng
 */