create user pesawatubes identified by pesawatubes;
grant connect to pesawatubes;
grant all privileges to pesawatubes;
connect pesawatubes/pesawatubes;

/* pesawat */
create table pesawat (
	id_pesawat number constraint pk_id_kereta primary key,
	depart TIMESTAMP not null, --keberangkatan
	arrive TIMESTAMP not null, --kedatangan
	price number not null,
	constraint ck_bk check(arrive > depart),
	constraint ck_harga check(price > 0)
);
create sequence seq_id_pesawat increment by 1;
	create trigger id_pesawat
		before insert on pesawat
		for each row
		begin
		if :new.id_pesawat is null then
		select seq_id_pesawat.nextval into :new.id_pesawat from dual;
		end if;
		end;
		/
insert into pesawat(depart,arrive,price) values (TIMESTAMP '2016-12-15 10:00:00',TIMESTAMP '2016-12-15 15:00:00',750000);
insert into pesawat(depart,arrive,price) values (TIMESTAMP '2016-12-15 12:00:00',TIMESTAMP '2016-12-15 15:00:00',650000);
insert into pesawat(depart,arrive,price) values (TIMESTAMP '2016-12-15 12:00:00',TIMESTAMP '2016-12-15 15:00:00',750000);
insert into pesawat(depart,arrive,price) values (TIMESTAMP '2016-12-15 12:00:00',TIMESTAMP '2016-12-15 15:00:00',850000);
insert into pesawat(depart,arrive,price) values (TIMESTAMP '2016-12-29 13:00:00',TIMESTAMP '2016-12-29 18:00:00',950000);

/* kabin */
create table kabin(
	id_kabin varchar2(20) primary key,
	id_pesawat varchar2(20) not null,
	seat varchar2(20) not null,
	class varchar2(20) not null,
	constraint fk_kabin_id_pesawat foreign key (id_pesawat) references pesawat(id_pesawat) on delete cascade
);
create sequence seq_id_gerbong increment by 1;
	create trigger id_kabin
		before insert on kabin
		for each row
		begin
		if :new.id_kabin is null then
		select seq_id_gerbong.nextval into :new.id_kabin from dual;
		end if;
		end;
		/
		insert into kabin(id_pesawat,seat,class) values ('QZ474','E','Eksekutif');
		insert into kabin(id_pesawat,seat,class) values ('QZ474','B','Bisnis');
		insert into kabin(id_pesawat,seat,class) values ('QZ474','A','Ekonomi');

		insert into kabin(id_pesawat,seat,class) values ('QZ632','E','Eksekutif');
		insert into kabin(id_pesawat,seat,class) values ('QZ632','B','Bisnis');
		insert into kabin(id_pesawat,seat,class) values ('QZ632','A','Ekonomi');

		insert into kabin(id_pesawat,seat,class) values ('QZ344','E','Eksekutif');
		insert into kabin(id_pesawat,seat,class) values ('QZ344','B','Bisnis');
		insert into kabin(id_pesawat,seat,class) values ('QZ344','A','Ekonomi');

		insert into kabin(id_pesawat,seat,class) values ('QZ689','E','Eksekutif');
		insert into kabin(id_pesawat,seat,class) values ('QZ689','B','Bisnis');
		insert into kabin(id_pesawat,seat,class) values ('QZ689','A','Ekonomi');

		insert into kabin(id_pesawat,seat,class) values ('QZ422','E','Eksekutif');
		insert into kabin(id_pesawat,seat,class) values ('QZ422','B','Bisnis');
		insert into kabin(id_pesawat,seat,class) values ('QZ422','A','Ekonomi');

	create table seat(
		id_seat varchar2(20) primary key not null,
		id_kabin varchar2(20) not null,
		status number not null,
		constraint fk_kb foreign key(id_kabin) references kabin(id_kabin) on delete set null
	);
	create sequence seq_id_seat increment by 1;

		create trigger id_seat
			before insert on seat
			for each row
		begin
			if :new.id_seat is null then
			select seq_id_seat.nextval into :new.id_seat from dual;
			end if;
		end;
		/
		insert into seat(id_kabin,id_seat,status) values ('QZ001','E1','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ001','E2','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ001','E3','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ001','E4','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ001','E5','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ001','E6','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ001','E7','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ001','E8','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ001','E9','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ001','E10','0');

		insert into seat(id_kabin,id_seat,status) values ('QZ002','B1','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ002','B2','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ002','B3','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ002','B4','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ002','B5','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ002','B6','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ002','B7','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ002','B8','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ002','B9','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ002','B10','0');

		insert into seat(id_kabin,id_seat,status) values ('QZ003','A1','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ003','A2','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ003','A3','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ003','A4','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ003','A5','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ003','A6','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ003','A7','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ003','A8','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ003','A9','0');
		insert into seat(id_kabin,id_seat,status) values ('QZ003','A10','0');

create table jadwal(
	id_jadwal number constraint p_j primary key not null,
	id_pesawat number not null,
	bandara_keberangkatan varchar2(30) not null,
	bandara_kedatangan varchar2(30) not null,
	constraint fk_2 foreign key(id_pesawat) references pesawat(id_pesawat),
	constraint ck_stsn check(bandara_keberangkatan!=bandara_kedatangan)
);
create sequence seq_id_jadwal increment by 1;
	create trigger id_jadwal
		before insert on jadwal
		for each row
		begin
		if :new.id_jadwal is null then
		select seq_id_jadwal.nextval into :new.id_jadwal from dual;
		end if;
		end;
		/

	insert into jadwal(id_pesawat,bandara_keberangkatan,bandara_kedatangan) values ('QZ344','Bandara Husein Sastranegara','Bandara Internasional Ngurah Rai');
	insert into jadwal(id_pesawat,bandara_keberangkatan,bandara_kedatangan) values ('QZ474','Bandara Internasional Soekarno-Hatta','Bandara Internasional Kuala Namu');
	insert into jadwal(id_pesawat,bandara_keberangkatan,bandara_kedatangan) values ('QZ482','Bandara Internasional Minangkabau','Bandara Internasional Soekarno-Hatta');
	insert into jadwal(id_pesawat,bandara_keberangkatan,bandara_kedatangan) values ('QZ632','Bandara Adi Sucipto','Bandara Husein Sastranegara');
	insert into jadwal(id_pesawat,bandara_keberangkatan,bandara_kedatangan) values ('QZ689','Bandara Internasional Juanda','Bandara Internasional Lombok Praya');
/* bandara */
create table bandara(
	id_bandara number primary key,
	nama_bandara varchar2(35) not null,
	kota varchar2(25) not null
);
create sequence seq_id_stasiun increment by 1;
create trigger id_bandara
	before insert on bandara
	for each row
begin
	if :new.id_bandara is null then
		select seq_id_stasiun.nextval into :new.id_bandara from dual;
	end if;
end;
/
insert into bandara(nama_bandara,kota) values ('Bandara Husein Sastranegara','Bandung');
insert into bandara(nama_bandara,kota) values ('Bandara Internasional Soekarno-Hatta','Jakarta');
insert into bandara(nama_bandara,kota) values ('Bandara Internasional Minangkabau','Padang');
insert into bandara(nama_bandara,kota) values ('Bandara Adi Sucipto','Yogyakarta');
insert into bandara(nama_bandara,kota) values ('Bandara Internasional Juanda','Surabaya');
insert into bandara(nama_bandara,kota) values ('Bandara Internasional Lombok Praya','Lombok');
insert into bandara(nama_bandara,kota) values ('Bandara Internasional Ngurah Rai','Bali');
insert into bandara(nama_bandara,kota) values ('Bandara Internasional Kuala Namu','Medan');

-- START HERE --------------------------------------------------------------------
create table booking (
	id_booking number constraint pk_pem primary key not null,
	nama_bandara varchar2(50) not null,
	email varchar2(35) not null,
	notelp varchar2(15) not null,
	alamat varchar2(60) not null,
	kode_pembayaran varchar2(20) unique,
	status varchar2(13) not null,
	constraint ck_eml CHECK (email like '%@%'),
	constraint in_stats CHECK( status IN ('Valid','Tidak Valid'))
);

create sequence seq_id_pemesan increment by 1;

create or replace trigger id_booking
		before insert on booking
		for each row
	begin
		if :new.id_booking is null then
			select seq_id_pemesan.nextval into :new.id_booking from dual;
		end if;
	end;
	/

/* PENUMPANG */
create table penumpang (
	id_penumpang number constraint idp primary key not null,
	id_booking number not null,
	nama_penumpang varchar2(25) not null,
	id_seat varchar2(7) not null,
	identitas varchar2(35) not null,
	status varchar2(13) not null,
	constraint fk_pesan foreign key(id_booking) references booking(id_booking) on delete set null,
	constraint stats check(status in('Valid','Tidak Valid'))
);

create sequence seq_id_penumpang increment by 1;
create or replace trigger id_penumpang
	before insert on penumpang
	for each row
begin
	if :new.id_penumpang is null then
		select seq_id_penumpang.nextval into :new.id_penumpang from dual;
	end if;
end;
/

create table tiket(
	no_tiket number primary key,
	id_pesawat number not null,
	id_kabin number not null,
	id_seat number not null,
	id_penumpang number not null,
	id_booking number not null,
	constraint fk_id_pesawat foreign key(id_pesawat) references pesawat(id_pesawat) on delete cascade,
	constraint fk_id_kabin foreign key(id_kabin) references kabin(id_kabin) on delete cascade,
	constraint fk_id_seat foreign key(id_seat) references seat(id_seat) on delete cascade,
	constraint fk_id_bkg foreign key(id_booking) references booking(id_booking) on delete cascade,
	constraint fk_id_p foreign key(id_penumpang) references penumpang(id_penumpang) on delete cascade
);

create sequence seq_id_tiket increment by 1;
create trigger id_tiket
	before insert on tiket
	for each row
begin
	if :new.no_tiket is null then
		select seq_id_tiket.nextval into :new.no_tiket from dual;
	end if;
end;
/

	create or replace function getEmpty(idg in number) return number as
		cursor cur_seat is select id_seat,status from seat where id_kabin = idg;
		data_seat cur_seat%rowtype;
		v_stats number;
	begin
		v_stats := 0;
		open cur_seat;
			loop
				fetch cur_seat into data_seat;
				if data_seat.status = 0 then
					v_stats := 1;
				end if;
				exit when cur_seat%notfound or v_stats = 1;
			end loop;
		close cur_seat;
		return(data_seat.id_seat);
	end getEmpty;
	/

	create or replace trigger stok
		after insert on penumpang
		for each row
	declare
		v_nama number;
	begin
		update seat set status = '1' where id_seat = :new.id_seat;
		select id_kabin into v_nama from seat where id_seat = :new.id_seat ;
		update kabin set seat = seat - 1 where id_kabin = v_nama;
	end;
	/

	create or replace trigger validasi
		after update on booking
		for each row
	declare
		cursor cur_penumpang is select id_penumpang,status from penumpang where id_booking = :old.id_booking;
		data_penumpang cur_penumpang%rowtype;
	begin
		if :new.status = 'Valid' then
			open cur_penumpang;
			loop
				fetch cur_penumpang into data_penumpang;
				exit when cur_penumpang%notfound;
				update penumpang set status = 'Valid' where id_penumpang = data_penumpang.id_penumpang and id_booking = :old.id_booking;
			end loop;
			close cur_penumpang;
		end if;
	end;
	/


create or replace function getseat(idp in number) return number as
	cursor cur_seat is select id_seat from penumpang where id_booking = idp;
	v_temp penumpang.id_seat%type;
	stats number;
	v_ret number;
begin
	stats := 0;
	open cur_seat;
		loop
			fetch cur_seat into v_temp;
			exit when cur_seat%notfound or stats = 1;

			if stats = 0 then
				v_ret := v_temp;
				stats := 1;
			end if;
		end loop;
	close cur_seat;
	return v_ret;
end getseat;
/

create or replace procedure printTiket(kode in varchar2) as
	cursor cur_penumpang is select id_penumpang,id_booking,id_seat from penumpang;
	data_penumpang cur_penumpang%rowtype;
	v_temp number;
	v_temp2 number;
	v_temp3 number;
	v_temp4 number;
	begin
	update booking set status = 'Valid' where kode_pembayaran = kode;
		select id_booking into v_temp from booking where kode_pembayaran = kode;
		select id_seat into v_temp2 from seat where id_seat = getseat(v_temp);
		select id_kabin into v_temp3 from kabin where id_kabin = (select id_kabin from seat where id_seat = v_temp2);
		select id_pesawat into v_temp4 from pesawat where id_pesawat = (select id_pesawat from kabin where id_kabin = v_temp3);
		open cur_penumpang;
		loop
		fetch cur_penumpang into data_penumpang;
		exit when cur_penumpang%notfound;
		if data_penumpang.id_booking = v_temp then
		insert into tiket(id_pesawat,id_kabin,id_seat,id_penumpang,id_booking) values (v_temp4,v_temp3,data_penumpang.id_seat,data_penumpang.id_penumpang,v_temp);
	end if;
	end loop;
	close cur_penumpang;
	end printTiket;
	/

COMMIT;