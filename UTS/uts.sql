 =========== BISMILLAH~ ========== */
/* Muhammad Naufal Fazanadi | 1505439 */

/* ---- INIT ---- */
connect system

create user uts identified by uts;

grant connect to uts;

grant all privileges to uts;

connect uts;
/* ------------- */

create table barang (
  id_barang number primary key,
  nama_barang varchar(30) not null,
  harga number(12) not null
);


create table pembeli(
  id_pembeli number primary key,
  nama_pembeli varchar(30) not null,
  telp_pembeli varchar(13) not null
);


create table kasir(
  id_kasir number primary key,
  nama_kasir varchar(30) not null,
  telp_kasir varchar(13) not null
);

create table transaksi(
  id_transaksi number primary key,
  id_pembeli number not null,
  id_kasir number not null,
  id_barang number not null,
  jumlah_beli number(3) not null,
  total_transaksi number(15),
  constraint fk_pembeli foreign key(id_pembeli) references pembeli(id_pembeli) on delete set null,
  constraint fk_kasir foreign key(id_kasir) references kasir(id_kasir) on delete set null,
  constraint fk_barang foreign key(id_barang) references barang(id_barang) on delete set null
);

create table bonus(
  no_bonus number primary key,
  id_pembeli number not null,
  bonus varchar(15) not null,
  constraint fk_id_pembeli foreign key(id_pembeli) references pembeli(id_pembeli) on delete set null
);

/* ------------------ NOMOR 1 ------------------ */
CREATE SEQUENCE counter_pembeli INCREMENT BY 1;

INSERT INTO pembeli VALUES (counter_pembeli.nextval,'Rylai','08123454542');
INSERT INTO pembeli VALUES (counter_pembeli.nextval,'Kael','081234112212');
INSERT INTO pembeli VALUES (counter_pembeli.nextval,'Traxex','085676555323');

CREATE SEQUENCE counter_kasir INCREMENT BY 1;

INSERT INTO kasir VALUES (counter_kasir.nextval,'Lanaya','08123456789');
INSERT INTO kasir VALUES (counter_kasir.nextval,'Rotundjere','087723454422');

CREATE SEQUENCE counter_barang INCREMENT BY 1;

INSERT INTO barang VALUES (counter_barang.nextval,'Mouse',100000);
INSERT INTO barang VALUES (counter_barang.nextval,'Keyboard',200000);
INSERT INTO barang VALUES (counter_barang.nextval,'Headphone',150000);
INSERT INTO barang VALUES (counter_barang.nextval,'Mouse Pad',50000);
INSERT INTO barang VALUES (counter_barang.nextval,'Laptop',10000000);

CREATE SEQUENCE counter_transaksi INCREMENT BY 1;

INSERT INTO transaksi VALUES (counter_transaksi.nextval,2,1,1,1,null);
INSERT INTO transaksi VALUES (counter_transaksi.nextval,3,2,2,2,null);
INSERT INTO transaksi VALUES (counter_transaksi.nextval,2,1,3,1,null);
INSERT INTO transaksi VALUES (counter_transaksi.nextval,1,1,4,3,null);
INSERT INTO transaksi VALUES (counter_transaksi.nextval,1,1,5,1,null);
INSERT INTO transaksi VALUES (counter_transaksi.nextval,2,2,4,5,null);
INSERT INTO transaksi VALUES (counter_transaksi.nextval,2,2,3,1,null);
INSERT INTO transaksi VALUES (counter_transaksi.nextval,1,2,2,2,null);
INSERT INTO transaksi VALUES (counter_transaksi.nextval,3,2,1,3,null);
INSERT INTO transaksi VALUES (counter_transaksi.nextval,3,1,2,2,null);
INSERT INTO transaksi VALUES (counter_transaksi.nextval,1,1,3,1,null);
INSERT INTO transaksi VALUES (counter_transaksi.nextval,2,1,4,1,null);
INSERT INTO transaksi VALUES (counter_transaksi.nextval,1,2,5,1,null);
INSERT INTO transaksi VALUES (counter_transaksi.nextval,1,1,1,5,null);

/*
SQL> select * from pembeli;

ID_PEMBELI NAMA_PEMBELI                   TELP_PEMBELI
---------- ------------------------------ -------------
         1 Rylai                          08123454542
         2 Kael                           081234112212
         3 Traxex                         085676555323

SQL> select * from kasir;

  ID_KASIR NAMA_KASIR                     TELP_KASIR
---------- ------------------------------ -------------
         1 Lanaya                         08123456789
         2 Rotundjere                     087723454422

SQL> select * from barang;

 ID_BARANG NAMA_BARANG                         HARGA
---------- ------------------------------ ----------
         1 Mouse                              100000
         2 Keyboard                           200000
         3 Headphone                          150000
         4 Mouse Pad                           50000
         5 Laptop                           10000000

SQL> select * from transaksi;

ID_TRANSAKSI ID_PEMBELI   ID_KASIR  ID_BARANG JUMLAH_BELI TOTAL_TRANSAKSI
------------ ---------- ---------- ---------- ----------- ---------------
           1          2          1          1           1
           2          3          2          2           2
           3          2          1          3           1
           4          1          1          4           3
           5          1          1          5           1
           6          2          2          4           5
           7          2          2          3           1
           8          1          2          2           2
           9          3          2          1           3
          10          3          1          2           2
          11          1          1          3           1

ID_TRANSAKSI ID_PEMBELI   ID_KASIR  ID_BARANG JUMLAH_BELI TOTAL_TRANSAKSI
------------ ---------- ---------- ---------- ----------- ---------------
          12          2          1          4           1
          13          1          2          5           1
          14          1          1          1           5

14 rows selected.
 */

/* ------------------ NOMOR 2 ------------------ */
CREATE TABLE rekap_pembeli AS
SELECT a.id_transaksi, b.nama_pembeli, c.nama_barang, d.nama_kasir, a.jumlah_beli
FROM transaksi a, pembeli b, barang c, kasir d
WHERE b.id_pembeli = a.id_pembeli AND c.id_barang = a.id_barang AND d.id_kasir = a.id_kasir
ORDER BY id_transaksi ASC;

/*
SQL> select * from rekap_pembeli;

ID_TRANSAKSI NAMA_PEMBELI                   NAMA_BARANG
------------ ------------------------------ ------------------------------
NAMA_KASIR                     JUMLAH_BELI
------------------------------ -----------
           1 Kael                           Mouse
Lanaya                                   1

           2 Traxex                         Keyboard
Rotundjere                               2

           3 Kael                           Headphone
Lanaya                                   1


ID_TRANSAKSI NAMA_PEMBELI                   NAMA_BARANG
------------ ------------------------------ ------------------------------
NAMA_KASIR                     JUMLAH_BELI
------------------------------ -----------
           4 Rylai                          Mouse Pad
Lanaya                                   3

           5 Rylai                          Laptop
Lanaya                                   1

           6 Kael                           Mouse Pad
Rotundjere                               5


ID_TRANSAKSI NAMA_PEMBELI                   NAMA_BARANG
------------ ------------------------------ ------------------------------
NAMA_KASIR                     JUMLAH_BELI
------------------------------ -----------
           7 Kael                           Headphone
Rotundjere                               1

           8 Rylai                          Keyboard
Rotundjere                               2

           9 Traxex                         Mouse
Rotundjere                               3


ID_TRANSAKSI NAMA_PEMBELI                   NAMA_BARANG
------------ ------------------------------ ------------------------------
NAMA_KASIR                     JUMLAH_BELI
------------------------------ -----------
          10 Traxex                         Keyboard
Lanaya                                   2

          11 Rylai                          Headphone
Lanaya                                   1

          12 Kael                           Mouse Pad
Lanaya                                   1


ID_TRANSAKSI NAMA_PEMBELI                   NAMA_BARANG
------------ ------------------------------ ------------------------------
NAMA_KASIR                     JUMLAH_BELI
------------------------------ -----------
          13 Rylai                          Laptop
Rotundjere                               1

          14 Rylai                          Mouse
Lanaya                                   5


14 rows selected.

 */

/* ------------------ NOMOR 3 ------------------ */
SELECT id_transaksi, nama_barang
FROM rekap_pembeli
WHERE nama_pembeli LIKE '%ex%';

/*
SQL> SELECT id_transaksi, nama_barang
  2  FROM rekap_pembeli
  3  WHERE nama_pembeli LIKE '%ex%';

ID_TRANSAKSI NAMA_BARANG
------------ ------------------------------
           2 Keyboard
           9 Mouse
          10 Keyboard

 */

/* ------------------ NOMOR 4 ------------------ */
CREATE OR REPLACE PROCEDURE prc_total_transaksi AS
  var_id transaksi.id_transaksi%type;
  var_idbarang transaksi.id_barang%type;
  var_harga barang.harga%type;
  var_jumlah transaksi.jumlah_beli%type;
  CURSOR c_id IS SELECT id_transaksi FROM transaksi;
BEGIN
  OPEN c_id;
  LOOP
    FETCH c_id INTO var_id;
    EXIT WHEN c_id%notfound;

    SELECT id_barang INTO var_idbarang FROM transaksi WHERE id_transaksi = var_id;
    SELECT harga INTO var_harga FROM barang WHERE id_barang = var_idbarang;
    SELECT jumlah_beli INTO var_jumlah FROM transaksi WHERE id_transaksi = var_id;

    UPDATE transaksi SET total_transaksi = (var_harga * var_jumlah) WHERE id_transaksi = var_id;

  END LOOP;
  CLOSE c_id;
END prc_total_transaksi;
/

EXEC prc_total_transaksi;

SELECT * FROM transaksi;
/*
SQL> SELECT * FROM transaksi;

ID_TRANSAKSI ID_PEMBELI   ID_KASIR  ID_BARANG JUMLAH_BELI TOTAL_TRANSAKSI
------------ ---------- ---------- ---------- ----------- ---------------
           1          2          1          1           1          100000
           2          3          2          2           2          400000
           3          2          1          3           1          150000
           4          1          1          4           3          150000
           5          1          1          5           1        10000000
           6          2          2          4           5          250000
           7          2          2          3           1          150000
           8          1          2          2           2          400000
           9          3          2          1           3          300000
          10          3          1          2           2          400000
          11          1          1          3           1          150000

ID_TRANSAKSI ID_PEMBELI   ID_KASIR  ID_BARANG JUMLAH_BELI TOTAL_TRANSAKSI
------------ ---------- ---------- ---------- ----------- ---------------
          12          2          1          4           1           50000
          13          1          2          5           1        10000000
          14          1          1          1           5          500000

14 rows selected.
 */

/* ------------------ NOMOR 5 ------------------ */
CREATE OR REPLACE TRIGGER tambah_barang
  AFTER
    UPDATE ON transaksi
  FOR EACH ROW
BEGIN
  UPDATE transaksi SET total_transaksi = (harga * :new.jumlah_beli) WHERE id_transaksi = :new.id_transaksi;
  SELECT total_transaksi FROM transaksi WHERE id_transaksi = :new.id_transaksi;
END;
/

/* ------------------ NOMOR 6 (SAMA KAYAK NOMOR 4) ------------------ */
CREATE OR REPLACE PROCEDURE prc_total_transaksi AS
  var_id transaksi.id_transaksi%type;
  var_idbarang transaksi.id_barang%type;
  var_harga barang.harga%type;
  var_jumlah transaksi.jumlah_beli%type;
  CURSOR c_id IS SELECT id_transaksi FROM transaksi;
BEGIN
  OPEN c_id;
  LOOP
    FETCH c_id INTO var_id;
    EXIT WHEN c_id%notfound;

    SELECT id_barang INTO var_idbarang FROM transaksi WHERE id_transaksi = var_id;
    SELECT harga INTO var_harga FROM barang WHERE id_barang = var_idbarang;
    SELECT jumlah_beli INTO var_jumlah FROM transaksi WHERE id_transaksi = var_id;

    UPDATE transaksi SET total_transaksi = (var_harga * var_jumlah) WHERE id_transaksi = var_id;

  END LOOP;
  CLOSE c_id;
END prc_total_transaksi;
/

EXEC prc_total_transaksi;

SELECT * FROM transaksi;
/*
SQL> SELECT * FROM transaksi;

ID_TRANSAKSI ID_PEMBELI   ID_KASIR  ID_BARANG JUMLAH_BELI TOTAL_TRANSAKSI
------------ ---------- ---------- ---------- ----------- ---------------
           1          2          1          1           1          100000
           2          3          2          2           2          400000
           3          2          1          3           1          150000
           4          1          1          4           3          150000
           5          1          1          5           1        10000000
           6          2          2          4           5          250000
           7          2          2          3           1          150000
           8          1          2          2           2          400000
           9          3          2          1           3          300000
          10          3          1          2           2          400000
          11          1          1          3           1          150000

ID_TRANSAKSI ID_PEMBELI   ID_KASIR  ID_BARANG JUMLAH_BELI TOTAL_TRANSAKSI
------------ ---------- ---------- ---------- ----------- ---------------
          12          2          1          4           1           50000
          13          1          2          5           1        10000000
          14          1          1          1           5          500000

14 rows selected.
*/

/* ------------------ NOMOR 7 ------------------ */
CREATE OR REPLACE FUNCTION get_kasir ()


/* ------------------ NOMOR 8 ------------------ */
CREATE OR REPLACE PROCEDURE prc_avg AS
  var_harga barang.harga%type;
  CURSOR c_harga IS SELECT harga FROM barang;

  summary NUMBER;
  divider NUMBER;
  ratarata NUMBER;
BEGIN
  summary := 0;
  divider := 0;

  OPEN c_harga;
  LOOP
    FETCH c_harga INTO var_harga;
    EXIT WHEN c_harga%notfound;

    summary := summary + var_harga;
    divider := divider + 1;

  END LOOP;
  CLOSE c_harga;

  ratarata := summary / divider;

  DBMS_OUTPUT.PUT_LINE(ratarata);
END prc_avg;
/

SET SERVEROUTPUT ON;

EXEC prc_avg;

/*
SQL> SET SERVEROUTPUT ON;
SQL> EXEC prc_avg;
2100000

PL/SQL procedure successfully completed.

 */

/* ------------------ NOMOR 9 ------------------ */
ALTER TABLE barang ADD jumlah_terjual NUMBER;
/*
SQL> desc barang;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 ID_BARANG                                 NOT NULL NUMBER
 NAMA_BARANG                               NOT NULL VARCHAR2(30)
 HARGA                                     NOT NULL NUMBER(12)
 JUMLAH_TERJUAL                                     NUMBER

 */

CREATE OR REPLACE TRIGGER first
  AFTER
    INSERT OR DELETE OR UPDATE ON transaksi
  FOR EACH ROW
BEGIN
  UPDATE barang SET jumlah_terjual = 0;
  IF INSERTING THEN
    UPDATE barang SET jumlah_terjual = jumlah_terjual + :new.jumlah_beli WHERE id_barang = :new.id_barang;
  ELSIF DELETING THEN
    UPDATE barang SET jumlah_terjual = jumlah_terjual - :old.jumlah_beli WHERE id_barang = :old.id_barang;
  ELSIF UPDATING THEN
    UPDATE barang SET jumlah_terjual = jumlah_terjual - :old.jumlah_beli WHERE id_barang = :old.id_barang;
    UPDATE barang SET jumlah_terjual = jumlah_terjual + :new.jumlah_beli WHERE id_barang = :new.id_barang;
  END IF;
END;
/
/*
SQL> UPDATE transaksi SET jumlah_beli = 5 WHERE id_transaksi = 1;

1 row updated.

SQL> select * from barang;

 ID_BARANG NAMA_BARANG                         HARGA JUMLAH_TERJUAL
---------- ------------------------------ ---------- --------------
         1 Mouse                              100000              4
         2 Keyboard                           200000              0
         3 Headphone                          150000              0
         4 Mouse Pad                           50000              0
         5 Laptop                           10000000              0

 */

/* ------------------ NOMOR 10 ------------------ */
ALTER TABLE pembeli ADD poin NUMBER;
/*
SQL> desc pembeli;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 ID_PEMBELI                                NOT NULL NUMBER
 NAMA_PEMBELI                              NOT NULL VARCHAR2(30)
 TELP_PEMBELI                              NOT NULL VARCHAR2(13)
 POIN                                               NUMBER

 */

CREATE OR REPLACE TRIGGER second
  AFTER
    INSERT OR DELETE OR UPDATE ON transaksi
  FOR EACH ROW
BEGIN
  UPDATE pembeli SET poin = 0;
  IF INSERTING THEN
    IF jumlah_beli > 10 THEN
      UPDATE pembeli SET poin = poin + :new.jumlah_beli WHERE id_pembeli = :new.id_pembeli;
    END IF;
  ELSIF DELETING THEN
    IF jumlah_beli > 10 THEN
      UPDATE pembeli SET poin = poin - :old.jumlah_beli WHERE id_pembeli = :old.id_pembeli;
    END IF;
  ELSIF UPDATING THEN
    IF jumlah_beli > 10 THEN
      UPDATE pembeli SET poin = poin - :old.jumlah_beli WHERE id_pembeli = :old.id_pembeli;
      UPDATE pembeli SET poin = poin + :new.jumlah_beli WHERE id_pembeli = :new.id_pembeli;
    END IF;
  END IF;
END;
/